<?php
require "bootstrap.php";
//use Chatter\Models\Message;
use Chatter\Models\User;
use Chatter\Models\Product;
//use Chatter\Middleware\Logging;

$app = new \Slim\App();
//$app->add(new Logging()); //one middleware

//----------------------------------------------
$app->get('/hello/{name}', function($request, $response,$args){
   return $response->write('Hello '.$args['name']);
});

$app->get('/customers/{cnumber}', function($request, $response,$args){
   return $response->write('Customer '.$args['cnumber']);
});

$app->get('/customers/{cnumber}/products/{pnumber}', function($request, $response,$args){
   return $response->write('Customer '.$args['cnumber' ]. ' Product '.$args['pnumber' ]);
});

//----------------------------------------------
//------------GET users------------------------
$app->get('/users', function($request, $response, $args){
    $_user = new User(); //create new user
    $users = $_user->all();
    $payload = [];
    foreach($users as $usr){
        $payload[$usr->id] = [
            'name'=>$usr->name,
            'phoneNumber'=>$usr->phoneNumber
        ];
    }
    return $response->withStatus(200)->withJson($payload);
});
//----------------------------------------------
//---------------CREATE users-----------------------

$app->post('/users', function($request, $response, $args){
       $name = $request->getParsedBodyParam('name','');
       $phoneNumber = $request->getParsedBodyParam('phoneNumber','');
       $_user = new User();
        $_user->name = $name;
        $_user->phoneNumber = $phoneNumber;
        $_user->save();
        if($_user->id){
            $payload = ['user id: '=>$_user->id];
            return $response->withStatus(201)->withJson($payload);
        }
        else{
            return $response->withStatus(400);
        }
     });

     //----------------------------------------------
     //--------------DELETE user-------------------------
     $app->delete('/users/{id}', function($request, $response,$args){
        $_user = User::find($args['id']);
        $_user->delete();
        if($_user->exists){
            return $response->withStatus(400);
        }
        else{
             return $response->withStatus(200);
        }
    });

    //----------------------------------------------
    //-----------GET user--------------------------
    $app->get('/users/{id}', function($request, $response,$args){
        $_id=$args['id'];
        $user = User::find($_id);
       return $response->withStatus(200)->withJson($user);
    });

    //----------------------------------------------
    //-------------UPDATE  user--------------------------
    $app->put('/users/{user_id}', function($request, $response, $args){
    $phoneNumber = $request->getParsedBodyParam('phoneNumber','');
    $name = $request->getParsedBodyParam('name','');
    $_user = User::find($args['user_id']);
    //שדות בטבלה
    $_user->phoneNumber = $phoneNumber;
    $_user->name = $name;
   
    if($_user->save()){
        $payload = ['user_id' => $_user->id,"result" => "The user has been updates successfuly"];
        return $response->withStatus(200)->withJson($payload);
    }
    else{
        return $response->withStatus(400);
    }
});

    //----------------------------------------------
    //------------AUTH---------------------------
    $app->post('/auth', function($request, $response,$args){
    
    $user = $request->getParsedBodyParam('user','');
    $password = $request->getParsedBodyParam('password','');
    //will need to go to DB
    if($user == 'jack' && $password == '1234'){
        //create JWT and sent to client
        $payload = ['token' => '12345'];
        return $response->withStatus(200)->withJson($payload);
    }
    else{ 
        $payload = ['token' => null]; 
        return $response->withStatus(403)->withJson($payload);
    }
}); 

   //----------------------------------------------
    //------------LOGIN---------------------------

   /* $app->post('/login', function($request, $response,$args){
    $name  = $request->getParsedBodyParam('name','');
    $password = $request->getParsedBodyParam('password','');    
    $_user = User::where('name', '=', $name)->where('password', '=', $password)->get();
    
    if($_user[0]->id){
        $payload = ['success'=>true];
        return $response->withStatus(201)->withJson($payload)->withHeader('Access-Control-Allow-Origin', '*');
    }
    else{
        return $response->withStatus(400)->withHeader('Access-Control-Allow-Origin', '*');
    }

});*/



//----------GET PRODUCTS---------------
$app->get('/products', function($request, $response, $args){
    $_product = new Product(); 
    $products = $_product->all();
    $payload = [];
    foreach($products as $prd){
        $payload[$prd->id] = [
            'name'=>$prd->name,
            'price'=>$prd->price
        ];
    }
    return $response->withStatus(200)->withJson($payload);
});

//----------GET PRODUCT---------------
 $app->get('/products/{id}', function($request, $response,$args){
        $_id=$args['id'];
        $product = Product::find($_id);
       return $response->withStatus(200)->withJson($product);
    });

//----------UPDATE PRODUCT---------------
$app->put('/products/{product_id}', function($request, $response, $args){
    $price = $request->getParsedBodyParam('price','');
    $name = $request->getParsedBodyParam('name','');
    $_product = Product::find($args['product_id']);
    //שדות בטבלה
    $_product->price = $price;
    $_product->name = $name;
   
    if($_product->save()){
        $payload = ['product_id' => $_product->id,"result" => "The product has been updates successfuly"];
        return $response->withStatus(200)->withJson($payload);
    }
    else{
        return $response->withStatus(400);
    }
});




$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});

$app->run();